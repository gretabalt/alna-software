﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        public const double oneminangle = 6;
        public const double onehourangle = 30; 

        static void Main(string[] args)
        {
            double first = 0;
            double second = 0;
            int hours;
            int minutes;

            // Irasymas
            Console.Write("Write hours:  ");

            String valandos = Console.ReadLine();

            while (!Int32.TryParse(valandos, out hours))      /* Programa vyksta, tol kol yra ivedamas teisingos valandos */
            {
                Console.WriteLine("Wrong time format. Please try again.");

                valandos = Console.ReadLine();
            }
            
            if (hours >= 24 || hours < 0)                /* Tikrinimas.Valandos privalo buti tarp 0 ir 24 */
            {
                Console.WriteLine("Wrong time format. Please try again");
                Console.ReadLine();
                return;                                 /* Kad programa toliau nebeveiktu */
            }

            Console.Write("\nWrite minutes:   ");
            
            String min = Console.ReadLine();

            while (!Int32.TryParse(min, out minutes))     /* Programa vyksta, tol kol yra ivedamas teisingos minutes */
            {
                Console.WriteLine("Wrong time format. Please try again.");

                min = Console.ReadLine();
            }

            if (minutes > 59 || minutes < 0)            /* Tikrinimas.Minutes privalo buti tarp 0 ir 60 */
            {
                Console.WriteLine("Wrong time format. Please try again");
                Console.ReadLine();
                return;                                 /* Kad programa toliau nebeveiktu */
            }

            //Kampo skaiciavimas
                    
            if (hours > 12)                             /* Tuo atveju, jei valandos yra ivedamos tarp 13 ir 24*/
            {
                hours = hours - 12;
            }

            double minutesangle = (double)minutes * oneminangle;                   /* Koks kampas minutines rodykles */
            double hoursangle = ((double)hours * onehourangle + ((double)minutes*30)/60);  /* Koks kampas valandines rodykles */

            if (minutesangle > hoursangle)              /* Skirtumas apskaiciuojamas is didesnio skaiciaus atimant mazesni */
            {
                first = minutesangle;
                second = hoursangle;
            }
            else
            {
                first = hoursangle;
                second = minutesangle;
            }

            double angle = first - second;

            if (angle > 180)                            /* Kad apskaiciuotu mazesni kampa tarp rodykliu */
            {
                angle = 360 - angle;
                Console.WriteLine("\nSmaller angle between arrows: ");
                Console.WriteLine(angle);
                Console.WriteLine("\nPress Enter to end");
                Console.ReadLine();
            }
            else
            {
                Console.WriteLine("\nSmaller angle between arrows: ");
                Console.WriteLine(angle);
                Console.WriteLine("\nPress Enter to end");
                Console.ReadLine();
            }
           
        }
    }
    
}

